from django.shortcuts import render, redirect, HttpResponse
from .forms import SignUpForm, EditProfile
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.contrib.auth import logout
from django.views.decorators.cache import cache_control
from location.models import Location
from el_pagination.decorators import page_template


@page_template('registration/location_list.html')
def home(request, template='registration/index.html', extra_context=None):
        place = Location.objects.order_by('-created')
        context = {'place': place, 'page_template': page_template}
        if extra_context is not None:
            context.update(extra_context)
        return render(request, template, context)


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@page_template('registration/location_list.html')
def welcome(request, template='registration/userhome.html', extra_context=None):
    if request.user.is_authenticated:
        place = Location.objects.order_by('-created')
        context = {'place': place, 'page_template': page_template}
        if extra_context is not None:
            context.update(extra_context)
        return render(request, template, context)
    else:
        return redirect('registration:signin')


def logout_view(request):
    logout(request)
    return redirect('registration:home')


def edit_profile(request):
    if request.method == 'POST':
        form = EditProfile(request.POST, request.FILES, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect('registration:userhome')
    else:
        form = EditProfile(instance=request.user)
        args = {'form': form}
        return render(request, 'registration/edit.html', args)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@page_template('registration/location_list.html')
def search(request, template='registration/userhome.html', extra_context=None):
    if request.user.is_authenticated:
        query = request.POST.get("search")
        place = Location.objects.order_by('-created')
        if query:
            place = place.filter(location__icontains=query)
            if place.count() == 0:
                return HttpResponse("sorry does not exist please add  <a href='/addplace'>here</a>")
        context = {'place': place, 'page_template': page_template}
        if extra_context is not None:
            context.update(extra_context)
        return render(request, template, context)
    else:
        return redirect('registration:signin')
