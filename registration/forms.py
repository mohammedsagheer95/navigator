from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30, required=False, help_text='Optional', widget=forms.TextInput(attrs={
        'class': 'form-control',
        }))
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional', widget=forms.TextInput(attrs={
        'class': 'form-control',
        }))
    last_name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={
        'class': 'form-control',
        }))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        }))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        }))
    email = forms.EmailField(max_length=30, required=False, help_text='Optional', widget=forms.TextInput(attrs={
        'class': 'form-control',
        }))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'image')


class EditProfile(UserChangeForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password', 'image')
