from django.db import models
from django.contrib.auth.models import User, AbstractUser
# Create your models here.


class User(AbstractUser):
    image = models.FileField(null=True, blank=True, default="user.png")
