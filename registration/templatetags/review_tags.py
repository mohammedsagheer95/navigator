from django import template
from location.models import Review, Location
from django.db.models import Avg

register = template.Library()


@register.simple_tag(takes_context=True)
def review_count(context, post_id):
    location = Location.objects.get(pk=post_id)
    rating = Review.objects.filter(location=location)
    if rating.count() > 0:
        return rating.count()
    else:
        return 0


@register.simple_tag(takes_context=True)
def review_average(context, post_id):
    location = Location.objects.get(pk=post_id)
    rate = Review.objects.filter(location=location).aggregate(Avg('rating'))
    if rate['rating__avg'] == None:
        rate['rating__avg'] = 0
    value = int(rate['rating__avg'])
    return value
