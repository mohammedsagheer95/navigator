from django.urls import path
from django.contrib.auth import views as auth_views
from . import views
from location import views as loc_views
app_name = 'registration'
urlpatterns = [
    path('', views.home, name='home'),
    path('signup', views.signup, name='signup'),
    path('signin', auth_views.login, name='signin'),
    path('userhome', views.welcome, name='userhome'),
    path('logout', views.logout_view, name='logout'),
    path('edit', views.edit_profile, name='edit'),
    path('addplace', loc_views.home, name='addplace'),
    path('search', views.search, name='search'),
    ]
