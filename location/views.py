#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.http import JsonResponse
from .forms import CreatePostModelForm, CreateReviewModelForm, CreateQuestionModelForm, CreateAnswerModelForm
from django.http import JsonResponse
from .models import Location, Review, Question, Answer
from django.db.models import Avg
from django.db import IntegrityError
from django.views.decorators.cache import cache_control


def home(request):
    form = CreatePostModelForm()
    context = {'form': form}
    return render(request, 'location/index.html', context)


def create_location(request):
    if request.method == 'POST':
        form = CreatePostModelForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()
            return redirect('registration:userhome')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def review(request, pk):
    if request.user.is_authenticated:
        questionform = CreateQuestionModelForm()
        place = Location.objects.get(pk=pk)
        question = Question.objects.filter(location=place).order_by('-created')
        reviews = Review.objects.filter(location=place).order_by('-created')
        rate = Review.objects.filter(location=place).aggregate(Avg('rating'))
        if reviews.count() > 0:
            avg_rating = rate['rating__avg']
        else:
            avg_rating = 0
        form = CreateReviewModelForm()
        return render(request, 'location/review.html', {
            'place': place,
            'reviews': reviews,
            'avg_rating': avg_rating,
            'form': form,
            'questionform': questionform,
            'question': question,
            })
    else:
        return redirect('registration:signin')


def userreview(request, pk):
    if request.method == 'POST':
        form = CreateReviewModelForm(request.POST)
        rating = request.POST.get('rating')

        if form.is_valid():
            try:
                review = form.save(commit=False)
                review.user = request.user
                place = Location.objects.get(pk=pk)
                review.location = place
                review.save()
                flag = 0
            except IntegrityError as e:
                e = "updated record"
                flag = 1
                print(e)
                Review.objects.filter(location=place, user=request.user).update(rating=request.POST.get('rating'), review=request.POST.get('review'))
    rate = Review.objects.filter(location=place).aggregate(Avg('rating'))
    st = '/review/' + str(pk)
    return JsonResponse({'message': str(int(rate['rating__avg'])) + " out of 5",
                            'pk': request.user.pk,
                            'flag': flag,
                            'rating': request.POST.get('rating'),
                            'addhtml': "<h3>by:" + str(request.user) + "</h3><img src=/media/" + str(request.user.image) + " style=height:60;width:60;><p><b>" + request.POST.get('review') + "</b></p>"})


def add_question(request, pk):
    if request.method == 'POST':
        form = CreateQuestionModelForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            location = Location.objects.get(pk=pk)
            post.location = location
            post.save()
        return JsonResponse({
            'question': "<a href='"+str(post.pk)+"'>"+str(request.POST.get('question'))+"</a><br>",
            })


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def view_answers(request, pk, qpk):
    if request.user.is_authenticated:
        question = Question.objects.get(pk=qpk)
        form = CreateAnswerModelForm()
        answers = Answer.objects.filter(question=question)
        return render(request, 'location/answers.html', {'question': question.pk, 'form': form, 'answers': answers, 'questiontext': question.question})
    else:
        return redirect('registration:signin')


def add_answers(request, pk, qpk):
    form = CreateAnswerModelForm(request.POST)
    if form.is_valid():
        print("this is a test")
        post = form.save(commit=False)
        post.user = request.user
        post.question = Question.objects.get(pk=qpk)
        post.save()
        st = '/review/' + str(pk)
    return JsonResponse({'answer': request.POST.get('answer')})
