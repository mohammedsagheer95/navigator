from .models import Location, Review, Question, Answer
from django import forms


class CreatePostModelForm(forms.ModelForm):
    location = forms.CharField(max_length=100, widget=forms.TextInput(attrs={
        'class': 'form-control',
        }))
    description = forms.CharField(required=True, max_length=300, widget=forms.Textarea(attrs={
        'class': 'form-control',
        }))
    class Meta:
        model = Location
        fields = ['location', 'description', 'image', 'category']


class CreateReviewModelForm(forms.ModelForm):
    CHOICES = [(1, '1 star'), (2, '2 star'), (3, '3 star'), (4, '4 star'), (5, '5 star')]
    rating = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect())

    class Meta:
        model = Review
        fields = ['review', 'rating']


class CreateQuestionModelForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = ['question']


class CreateAnswerModelForm(forms.ModelForm):

    class Meta:
        model = Answer
        fields = ['answer']
