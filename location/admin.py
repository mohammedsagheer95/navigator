from django.contrib import admin
from .models import Location, Review, Question, Answer

# Register your models here.
admin.site.register(Location)
admin.site.register(Review)
admin.site.register(Question)
admin.site.register(Answer)
