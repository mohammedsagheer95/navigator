from django.urls import path
from . import views
app_name = 'location'
urlpatterns = [
    path('insert', views.create_location, name='insert'),
    path('review/<int:pk>/', views.review, name='review'),
    path('review/<int:pk>/userreview', views.userreview, name='userreview'),
    path('review/<int:pk>/addquestion', views.add_question, name='addquestion'),
    path('review/<int:pk>/<int:qpk>', views.view_answers, name='viewanswers'),
    path('review/<int:pk>/<int:qpk>/addanswer', views.add_answers, name='addanswers'),

    ]
