from django.db import models
from django.utils import timezone
import datetime
from registration.models import User
from django.core.validators import MaxValueValidator, MinValueValidator


class Location(models.Model):
    CATEGORY_CHOICES = [
    ('Home', 'Home'),
    ('Mall', 'Mall'),
    ('Metro station', 'Metro station'),
    ('Tourist spot', 'Tourist spot'),
    ('Food Outlet', 'Food Outlet'),
    ('Hospital', 'Hospital'),
    ]
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField(max_length=250, blank=True)
    location = models.CharField(max_length=50, blank=True)
    created = models.DateTimeField('date published', default=timezone.now)
    image = models.FileField(null=True, blank=True)
    category = models.CharField(max_length=50, choices=CATEGORY_CHOICES, blank=True)

    def __str__(self):
        return self.location


class Review(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    review = models.TextField(max_length=500, blank=True)
    rating = models.IntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)])
    created = models.DateTimeField('date published', default=timezone.now)

    class Meta:
        unique_together = ('user', 'location')

    def __str__(self):
        return "review for " + str(self.location)


class Question(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    question = models.TextField(max_length=200, blank=True)
    created = models.DateTimeField('date published', default=timezone.now)

    def __str__(self):
        return str(self.question) + " @" + str(self.location)


class Answer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.TextField(max_length=200, blank=True)

    def __str__(self):
        return self.answer
